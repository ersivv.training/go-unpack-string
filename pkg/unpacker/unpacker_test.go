package unpacker

import "testing"

func TestUnpack(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: `ab3c0d\1`,
			args: args{s: `ab3c0d\1`},
			want: `abbbd1`,
		},
		{
			name: `ж3d1`,
			args: args{s: `ж3d1`},
			want: `жжжd`,
		},
		{
			name: `\\1`,
			args: args{s: `\\1`},
			want: `\1`,
		},
		{
			name: `\1\2k`,
			args: args{s: `\1\2k`},
			want: `12k`,
		},
		{
			name: `a10bc2`,
			args: args{s: `a10bc2`},
			want: `aaaaaaaaaabcc`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Unpack(tt.args.s); got != tt.want {
				t.Errorf("Unpack() = %v, want %v", got, tt.want)
			}
		})
	}
}
