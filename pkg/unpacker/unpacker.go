package unpacker

import (
	"log"
	"strconv"
	"strings"
	"unicode"
)

func init() {
	log.SetFlags(log.Lshortfile)
}

func Unpack(s string) string {

	r := []rune(s)
	l := len(r)
	// log.Printf("l=%d\n", l)

	var sb strings.Builder

	for i := 0; i < l; i++ {
		// log.Printf("%d: %s\n", i, string(r[i]))

		if r[i] == rune('\\') {
			if unicode.IsDigit(r[i+1]) {
				sb.WriteRune(r[i+1])
				i++
			} else {
				sb.WriteRune(r[i])
			}
			continue
		}

		cursor := i
		for i < l-1 {
			if unicode.IsDigit(r[i+1]) {
				i++
				continue
			}
			break
		}
		if i > cursor {
			// log.Printf("string(r[%d:%d])=%s", cursor+1, i, string(r[cursor+1:i+1]))
			n, _ := strconv.Atoi(string(r[cursor+1 : i+1]))
			sb.WriteString(strings.Repeat(string(r[cursor]), n))
			continue
		}

		sb.WriteRune(r[i])

	}
	return sb.String()
}
