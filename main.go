package main

import (
	"fmt"
	"unpacker/pkg/unpacker"
)

func main() {

	s := `ab3c0d\1`
	fmt.Printf("%s -> %s\n", s, unpacker.Unpack(s))

	s = `ж3d1`
	fmt.Printf("%s -> %s\n", s, unpacker.Unpack(s))

	s = `a11b2cd`
	fmt.Printf("%s -> %s\n", s, unpacker.Unpack(s))

}
